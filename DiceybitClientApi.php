<?php
/**
 * Created by DiceyBit Team
 * User: com.diceybit
 * Date: 3/18/18
 * Time: 18:18
 *
 * @author DiceyBit Team
 * @copyright DiceyBit.com
 *
 */

namespace DiceyBit;


class ClientAPI
{

	/**
	 * @var string
	 */
	protected $apiUrl = 'https://integration.diceybit.com/api';
	/**
	 * @var string
	 */
	protected $apiVer = "1.1";
	/**
	 * @var null
	 */
	private $partnerId = null;
	/**
	 * @var null
	 */
	private $apiSecret = null;
	/**
	 * @var null
	 */
	private $request = null;
	/**
	 * @var null
	 */
	private $response = null;
	/**
	 * @var null|string
	 */
	private $lang = null;



	/**
	 * Returns info aout withdrawal of bitcoins to partner
	 *
	 * @param $transaction_id
	 * @return mixed
	 */
	public function btc_transaction($transaction_id)
	{
		return $this->getWallet("btc_transaction",
			array(
				"id"=>$transaction_id,
			)
		);
	}

	/**
	 * Returns info aout withdrawal of bitcoins to partner
	 *
	 * @param $withdraw_address
	 * @param $satoshi_fee
	 * @return mixed
	 */
	public function withdraw_btc($withdraw_address, $satoshi_fee = 1500)
	{
		return $this->getWallet("withdraw_btc",
			array(
				"address"=>$withdraw_address,
				"fee"=>$satoshi_fee,
				"currency"=>"btc"
			)
		);
	}

	/**
	 * Returns address of new BTC wallet
	 *
	 * @param $user_id
	 * @return mixed
	 */
	public function new_btc_address($user_id)
	{
		return $this->getWallet("new_btc_address",
			array(
				"user_id"=>$user_id,
				"currency"=>"btc"
			)
		);
	}

	/**
	 * Returns iframe source url for Lottery game
	 *
	 * @param $user_id
	 * @param $currency
	 * @param int $minBet
	 * @param int $maxBet
	 * @param int $demo
	 * @param $home_url
	 * @return mixed
	 */
	public function getLottery3x3Frame ($user_id, $currency, $demo = 0, $minBet = 0, $maxBet = 1, $home_url = '')
	{
		return $this->getGameFrame("lottery3x3",
			array(
				"user_id"=>$user_id,
				"currency"=>$currency,
				"minbet"=>$minBet,
				"maxbet"=>$maxBet,
				"is_demo"=>$demo,
				"home_url"=>$home_url,
				"lang"=>$this->lang
			)
		);
	}

	/**
	 * Returns iframe source url for Slot game
	 *
	 * @param $slot_id
	 * @param $user_id
	 * @param $currency
	 * @param int $minBet
	 * @param int $maxBet
	 * @param int $demo
	 * @param $home_url
	 * @return mixed
	 */
	public function getSlotMachineFrameId ($slot_id, $user_id, $currency, $demo = 0, $minBet = 0, $maxBet = 1, $home_url = '')
	{
		return $this->getGameFrame($slot_id,
			array(
				"user_id"=>$user_id,
				"currency"=>$currency,
				"minbet"=>$minBet,
				"maxbet"=>$maxBet,
				"is_demo"=>$demo,
				"home_url"=>$home_url,
				"lang"=>$this->lang
			)
		);
	}

	/**
	 * Returns iframe source url for Slot game
	 *
	 * @param $user_id
	 * @param $currency
	 * @param int $minBet
	 * @param int $maxBet
	 * @param int $demo
	 * @param $home_url
	 * @return mixed
	 */
	public function getSlotMachineFrame ($user_id, $currency, $demo = 0, $minBet = 0, $maxBet = 1, $home_url = '')
	{
		return $this->getGameFrame("slot_machine",
			array(
				"user_id"=>$user_id,
				"currency"=>$currency,
				"minbet"=>$minBet,
				"maxbet"=>$maxBet,
				"is_demo"=>$demo,
				"home_url"=>$home_url,
				"lang"=>$this->lang
			)
		);
	}

	/**
	 * Returns iframe source url for Classic game
	 *
	 * @param $user_id
	 * @param $currency
	 * @param int $minBet
	 * @param int $maxBet
	 * @param float $houseEdge
	 * @param int $demo
	 * @param $home_url
	 * @return mixed
	 */
	public function getClassicDiceFrame ($user_id, $currency, $demo = 0,  $houseEdge = 0.02, $minBet = 0, $maxBet = 1, $home_url = '')
    {
        return $this->getGameFrame("classic_dice",
			array(
				"user_id"=>$user_id,
				"currency"=>$currency,
				"minbet"=>$minBet,
				"maxbet"=>$maxBet,
				"houseedge"=>$houseEdge,
				"is_demo"=>$demo,
				"home_url"=>$home_url,
				"lang"=>$this->lang
			)
		);
    }

	/**
	 * Returns btc payment results
	 *
	 * @param $action
	 * @param $params
	 * @return mixed
	 */
	private function getWallet ($action, $params)
	{
		return $this->apiPost($this->getEndpointURL("merchant/$action"), $params);
	}

	/**
	 * Returns iframe source url for $gameId game
	 *
	 * @param $gameBundle
	 * @return mixed
	 */
	private function getGameFrame ($gameBundle, $params)
	{
		return $this->apiPost($this->getEndpointURL("get_frame/$gameBundle"), $params);
	}

	/**
	 * Determinate security status of array
	 *
	 * @param $post
	 * @return bool
	 */
	public function isDataSecure($post)
	{
		$temp = $post;
		$sign = $temp['sign'];
		unset($temp['sign']);

		return $this->getSecureHash($temp) === $sign;
	}

	/**
	 * @return null
	 */
	public function getResponse()
	{
		return $this->response;
	}

	/**
	 * @param $user_id
	 * @param $balance
	 * @param null $nickname
	 * @param null $avatar
	 * @return string
	 */
	public function getFrameCallback($user_id, $balance, $nickname = null, $avatar = null)
	{
		$data = array();
		$data['user_id'] = $user_id;
		$data['balance'] = $balance;
		$nickname?($data['nickname'] = $nickname):false;
		$avatar?($data['avatar'] = $avatar):false;

		return $this->callbackAnswer($data);
	}
	/**
	 * @param $data
	 * @return string
	 */
	private function callbackAnswer($data)
	{
		$data = $this->secureRequest($data);
		return json_encode($data);
	}
	/**
	 * Construct endpoint url
	 *
	 * @param $endpoint
	 * @return string
	 */
	private function getEndpointURL ($endpoint)
	{
		return $this->apiUrl ."/". $endpoint . "/";
	}

	/**
	 * Makes post request to server URL
	 *
	 * @param $url
	 * @param $data
	 * @param null $optional_headers
	 * @return mixed
	 * @throws \Exception
	 */
	private function apiPost ($url, $data, $optional_headers = null)
    {
		$url .= "?".http_build_query(array("ver"=>$this->apiVer, "partner_id"=>$this->partnerId)); // for good logging procedure
		$data = $this->secureRequest($data);

		$data = http_build_query($data);
        $params = array('http' => array(
            'method' => 'POST',
            'content' => $data
        ));

        if ($optional_headers !== null)
        {
            $params['http']['header'] = $optional_headers;
        }
        $ctx = stream_context_create($params);
		$this->request = $ctx;

        $fp = @fopen($url, 'rb', false, $ctx);
        if (!$fp) {
            throw new \Exception("Problem with sending API");
        }
        $response = @stream_get_contents($fp);
        if ($response === false)
        {
            throw new \Exception("Problem reading data from $url");
        }
		$this->response = $response;
        $object = json_decode($response, true);

		$this->handleErrors($object);

        if (!$this->isDataSecure($object)) throw new \Exception("Received data is not secured ".$this->response, 12400);

        return $object;
    }

	/**
	 * @param $data
	 * @return array
	 */
	private function secureRequest($data)
	{
		$sign = $this->getSecureHash($data);
		$data += array("sign"=>$sign);

		return $data;
	}

	/**
	 * Generates secured string to sign post request
	 *
	 * @param $data
	 * @return string
	 */
	private function getSecureHash($data)
	{
		ksort($data);
		$s = "";
		foreach ($data as $k => $v)
		{
			$s .= $k.$v;
		}
		return sha1($s.$this->apiSecret);
	}

	/**
	 * Errors handler
	 *
	 * @param $object
	 * @throws \Exception
	 */
	private function handleErrors($object)
	{

		if (!$object['success'])
		{
			throw new \Exception($object['message'], intval($object['code']));
		}
	}

	/**
	 *
	 */
	public function debugLast()
	{
		print_r("request", $this->request);
		print_r("response", $this->response);
	}

	function __construct($partnerId, $secretKey, $lang = "en")
    {
        $this->partnerId = $partnerId;
        $this->apiSecret = $secretKey;
        $this->lang = $lang;
    }

    function __destruct()
    {

    }
}