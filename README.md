## HOW TO USE


**1. Initialization** 
``` 
use DiceyBit; 


$ClientAPI = new ClientAPI({API_ID},"{API_SECRET_KEY}");

```
{API_ID} – integer partner number,
{API_SECRET_KEY} – string secret key

**2. Request iframe URL** with getClassicDiceFrame();

```

//Connect user to game
$obj_frame = $ClientAPI->getClassicDiceFrame($server_user_id, "btc");

//Get iframe url
$url = $obj_frame["url"];

// echo iframe

echo '<br /><iframe src="'.$url.'" width="460" height="760"></iframe>';

```



